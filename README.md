
```go
	import "sun/obf"

	buf := make([]byte, 32)
	rand.Read(buf)
	encKey := string(buf)
	if err := Obfuscate("darwin", encKey, "locker", "out.mac", false, false); err != nil {
		AdminAlert(err)
	}
```
