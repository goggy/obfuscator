package obf

import (
	"fmt"
	"go/build"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

func Obfuscate(OS, encKey, pkgName, outPath string, winHide, noStaticLink, clean bool) error {
	newGopath, err := ioutil.TempDir("", "")
	if err != nil {
		return fmt.Errorf("Failed to create temp dir:", err)
	}
	if clean {
		defer os.RemoveAll(newGopath)
	}
	log.Println("Copy path", pkgName)
	if err := CopyGopath(pkgName, newGopath, false); err != nil {
		return fmt.Errorf("Failed to copy into a new GOPATH:", err)
	}

	enc := &Encrypter{Key: encKey}
	if err := ObfuscatePackageNames(newGopath, enc); err != nil {
		return fmt.Errorf("Failed to obfuscate package names:", err)
	}
	if err := ObfuscateStrings(newGopath); err != nil {
		return fmt.Errorf("Failed to obfuscate strings:", err)
	}
	if err := ObfuscateSymbols(newGopath, enc); err != nil {
		return fmt.Errorf("Failed to obfuscate symbols:", err)
	}

	ctx := build.Default

	newPkg := pkgName
	newPkg = encryptComponents(pkgName, enc)

	ldflags := `-ldflags=-s -w`
	if winHide {
		ldflags += " -H=windowsgui"
	}
	if !noStaticLink {
		ldflags += ` -extldflags "-static"`
	}

	goCache := newGopath + "/cache"
	os.Mkdir(goCache, 0755)

	arguments := []string{"build", ldflags, "-o", outPath, newPkg}
	environment := []string{
		"GOROOT=" + ctx.GOROOT,
		"GOARCH=" + ctx.GOARCH,
		"GOOS=" + OS,
		"GOPATH=" + newGopath,
		"PATH=" + os.Getenv("PATH"),
		"GOCACHE=" + goCache,
	}

	cmd := exec.Command("go", arguments...)
	cmd.Env = environment
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("Failed to compile:", err)
	}

	return nil
}

func encryptComponents(pkgName string, enc *Encrypter) string {
	comps := strings.Split(pkgName, "/")
	for i, comp := range comps {
		comps[i] = enc.Encrypt(comp)
	}
	return strings.Join(comps, "/")
}
